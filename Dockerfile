FROM node:10


WORKDIR /usr/app


COPY package*.json ./


RUN apt-get update  &&\
    apt-get install -y ffmpeg &&\
    npm install

COPY . .

ENV GOOGLE_APPLICATION_CREDENTIALS="/usr/app/moois-covid19-4eece823397d.json"

EXPOSE 5002
CMD [ "node", "index.js" ]