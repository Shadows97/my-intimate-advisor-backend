const { Translate } = require('@google-cloud/translate').v2;
const lang = require('../constants/langs');

const translate = new Translate();


const range = (min, max) => {  
    return Math.floor(
      Math.random() * (max - min + 1) + min
    )
}


const autoDetectLang = async (text) => {
  let [detections] = await translate.detect(text);
  detections = Array.isArray(detections) ? detections : [detections];
  //console.log(detections[0].language)
  return detections[0].language
}


const translateText = async (text, target) => {
  let [translations] = await translate.translate(text, target);
  translations = Array.isArray(translations) ? translations : [translations];
  console.log(translations)
  return translations[0]

}



const checkResponseLang = async (text, language) => {
  let resp = text
  console.log(text)
  if (language == lang.EN) {
    resp = await translateText(text, lang.EN)
    //console.log(resp)
  }
  return resp
}


  

module.exports = {
  range,
  autoDetectLang,
  translateText,
  checkResponseLang,
}