const BONJOUR_RESPONSE  = ["bonjour chérie, ça fais du bien de t'entendre. Que puis je faire pour toi?", "Bonjour chérie, Contente de t'entendre. dis moi qu'a tu?", "Salut, je suis heureuse de te parler. qu'est ce qui se passe ?" ]
const AGE_SEXUALITE_RESPONSE  = "Il n’y a pas d’âge précis pour commencer à avoir une vie sexuelle mais le mieux serait d’atteindre l’âge de la majorité pour pouvoir entamer ce qu’on appelle une vie sexuelle,le meilleur serait d’attendre d’être marié avant toute chose pour éviter certaines conséquences tels que les grossesses non désirées,les infections sexuellement transmissible. OK chérie ?";
const BONSOIR_RESPONSE = ["bonsoir, j'espère que tu vas bien. Comment puis-je t'aider?", "Salut, contente de t'entendre. Que puis-je faire pour toi?", "Coucou, qu'est ce que je peux faire pour toi.", "Bonsoir chérie ! j’espère que tu vas bien. Que me vaut l’honneur de ton appel ?"];
const CAUSE_DES_PERTE_BLANCHE_RESPONSE = "Les pertes blanches sont un phénomène naturel car au moment de la puberté, environ un an avant les premières règles, les pertes blanches apparaissent. Elles sont produites par l’organisme et, plus précisément, par le vagin et l’utérus.";
const CAS_DES_REGLE_DOULOUREUSE_RESPONSE = "Ces douleurs sont le fait des muscles de l'utérus qui se contractent pour expulser l'ovule non fécondé ainsi que les cellules et le sang de l'endomètre (paroi utérine) qui ont servi à le nourrir.  Néanmoins, les dysménorrhées peuvent avoir pour origine une pathologie organique. Prenez conseil auprès de votre médecin.";
const CAUSE_INFECTION_VAGINALE_RESPONSE = "Le stress, manque de sommeil, grossesse, diabète, consommation abondante de sucre et plein d’autres choses.";
const COMMENT_GUERIR_UNE_INFECTION_RESPONSE = "Essayer de faire la toilette avec un savon alcalin type savon de Marseille; Ensuite mettre une crème sur la vulve et le vagin ; Traiter le partenaire si besoin ; Mettre des sous-vêtements en coton ;  Attendre que les brûlures et démangeaisons soient passées pour reprendre les rapports.  Si après l’application de ces solutions citées ci-dessus la douleur ou les maux sont persistant ou répétitif, alors je te conseille de consulter un médecin.";
const CONSEIL_MEDICAL_REGLE_DOULOUREUSE_RESPONSE = "Les médicaments anti inflammatoires non-stéroidiens tels que l'ibuprofène sont plus efficace pour soulager les règles douloureuses.";
const CONSEQUENCE_DE_LA_SEXUALITE_RESPONSE = "Avoir des rapports sexuels même très fréquent (plusieurs fois par jour) n'entraîne aucun problème pour la santé. De même avoir des rapports sexuels très espacés ou bien pas de rapport sexuels du tout n'entraîne pas non plus de problèmes \
                                            Pour éviter certaines situations désagréables mieux vaut pratiquer l’abstinence sexuelle";
const CONSEQUENCE_DES_REGLE_DOULOUREUSE_RESPONSE = "Des modifications de l'humeur peuvent apparaître plusieurs jours avant le début des règles. Certaines femmes en souffrent - à divers degrés -, d'autres non. Ces perturbations font partie d'un ensemble de symptômes appelés syndrome prémenstruel. On y trouve de l'irritabilité, de la tristesse, des difficultés à se concentrer ; mais aussi des maux de tête, des nausées… De la même façon, juste avant les règles, le corps à tendance à retenir l'eau. Voilà pourquoi on se sent gonflée et que les seins sont tendus et sensibles.";
const DEFINITION_REGLE_DOULOUREUSE_RESPONSE = "On parle de dysménorrhée ou de règles douloureuses  lorsqu'une femme ressent des douleurs avant et au début de ses règles. Elles sont plus intenses lorsque le flux est plus important. La dysménorrhée apparaît soit avant les règles, soit pendant, mais se produit le plus souvent vers le deuxième jour pour s'intensifier progressivement avant de disparaître.";
const DIFFERENCE_MST_ET_IST_RESPONSE = "la seule différence entre une IST et une MST concerne la terminologie utilisée. Les sigles IST et MST désignent les mêmes maladies.";
const DEPISTAGE_MST_OU_IST_RESPONSE = "Un test de dépistage des IST est recommandé en cas de doute ou de rapport sexuel à risque. Ce dépistage est d’autant plus important qu’il est possible d’être porteur d’une IST sans s’en rendre compte.";
const EVITER_LES_PERTE_BLANCHE_RESPONSE = ["Éviter les pertes blanches n’est pas du tout impossible d’où il te faudra, Consommez chaque matin après le petit déjeuner, une gousse d'ail. Ce n'est pas toujours accommodant à cause de l'odeur car l'ail est l'antibiotique le plus puissant en tant qu'aliment. A la place, vous pourriez également prendre une capsule d'ail désodorisée que vous trouverez dans les pharmacies.",
                                        "Prenez un demi-litre d'eau et portez- le à ébullition. Ensuite, prenez une gousse d'ail et émincez- la. Plongez la ainsi émincée dans le demi-litre d'eau bouillante. Si l'infection a duré depuis un bon moment, il est recommandé d'utiliser deux gousses d'ail. Ensuite, prenez une poire à lavement. Puis mettez dedans la lotion obtenue. Et faites un lavement régulier le soir. Renouvelez l'opération jusqu'à obtention de résultat, la plupart du temps au bout de quinze jours. Après traitement, pour parer à toute éventuelle récidive, faites deux fois par semaine, des douches vaginales toujours à base d'ail. Mais cette fois - ci, une seule gousse suffit, toujours dans un demi-litre d'eau bouillante."];

const FAIRE_SA_TOILETTE_RESPONSE = "Pour effectuer votre toilette intime, n’utilisez pas de gant de toilette (susceptible de contenir des germes) mais vos mains propres (lavez–les avant de nettoyer la zone intime). \
                                    Évitez les antiseptiques moussants comme les cosmétiques parfumés (gels, savons, produits pour le bain ou d’hygiène intime).\
                                    Après la toilette, n'utilisez pas de parfums ou déodorants.";
const GROSSESSESSE_PRECOCE_RESPONSE = ["Une grossesse est dite précoce lorsqu’elle est portée par une jeune fille ayant moins de 18 ans, parfois 20 ans.",
                                        "Grossesse précoce est une grossesse qui survient chez la jeune fille avant le temps c'est à dire avant l'âge de 18 ans ou 20 ans et qui cause des problèmes de santé. Elle peut être désirée ou non. Une grossesse non désirée est une grossesse non voulue, qui peut aussi causer des problèmes de santé."];
const INFECTION_VAGINALE_RESPONSE = "Une fois l'infection vaginale déclarée, les symptômes ne    trompent pas : Démangeaisons permanentes de la vulve et de l'entrée du vagin ; Pertes blanches épaisses et crémeuses Brûlures vaginales pendant les mictions ; Rapports sexuels de plus en plus douloureux ; Vulve rouge vif et gonflée. Tu auras l’un de ces symptômes si tu as une infection vaginale.";
const LA_CONTRACTION_MST_RESPONSE = "Les MST peuvent être transmises lors d’un rapport sexuel, quel que soit son type, entre deux partenaires. Elles sont souvent diagnostiquées chez de jeunes adultes. Certaines IST peuvent également être transmises de la mère à l’enfant.";
module.exports = {
    BONJOUR_RESPONSE,
    AGE_SEXUALITE_RESPONSE,
    BONSOIR_RESPONSE,
    CAUSE_DES_PERTE_BLANCHE_RESPONSE,
    CAS_DES_REGLE_DOULOUREUSE_RESPONSE,
    CAUSE_INFECTION_VAGINALE_RESPONSE,
    COMMENT_GUERIR_UNE_INFECTION_RESPONSE,
    CONSEIL_MEDICAL_REGLE_DOULOUREUSE_RESPONSE,
    CONSEQUENCE_DE_LA_SEXUALITE_RESPONSE,
    CONSEQUENCE_DES_REGLE_DOULOUREUSE_RESPONSE,
    DEFINITION_REGLE_DOULOUREUSE_RESPONSE,
    DIFFERENCE_MST_ET_IST_RESPONSE,
    DEPISTAGE_MST_OU_IST_RESPONSE,
    EVITER_LES_PERTE_BLANCHE_RESPONSE,
    FAIRE_SA_TOILETTE_RESPONSE,
    GROSSESSESSE_PRECOCE_RESPONSE,
    INFECTION_VAGINALE_RESPONSE,
    LA_CONTRACTION_MST_RESPONSE
}