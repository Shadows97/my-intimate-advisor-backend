

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
var cors = require('cors');
var services = require('./services/sttService');
var multer  = require('multer');
var upload = multer({ dest: 'public/'});



app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true,}));

app.use(upload.array(['file'])); 


app.post("/stt", services.speechToText);

app.post('/bot', services.botResponse );

const port = process.env.PORT || 5002;

app.listen(port, () => console.log(`Server running on port ${port} 🔥`));
