const axios = require('axios');
const speech = require('@google-cloud/speech');
const {Wit, log} = require('node-wit');
const fs = require('fs');
const ffmpeg = require('fluent-ffmpeg');

const {Translate} = require('@google-cloud/translate').v2;

const translate = new Translate();

const key = require('../constants/secureKeys');
const utiles = require('../utils/utils');
const returnResponse = require('../constants/response');
const intents = require('../constants/intents');
const lang = require('../constants/langs');


ffmpeg.setFfmpegPath("/usr/bin/ffmpeg");


const clientWit = new Wit({
  accessToken: key.MY_TOKEN,
  logger: new log.Logger(log.DEBUG)
});





const speechToText = async (request, response) => {
    let respo = "";

    console.log(request.files)
  
    // Creates a client
    const client = new speech.SpeechClient();
  
   

    var tmp_path = request.files[0].path;
    var fileName = "public/" + request.files[0].originalname;
    var fileName2 = "upload/" + request.files[0].originalname;

    var src = fs.createReadStream(tmp_path);
    var dest = fs.createWriteStream(fileName);
    src.pipe(dest);
  src.on('end', async function () {



    ffmpeg().input(fileName)
      .outputOptions([
        '-f s16le',
        '-acodec pcm_s16le',
        '-vn',
        '-ac 1',
        '-ar 41k',
        '-map_metadata -1'
      ])
      // here we save our result to the encodedPath we declared above
      .save(fileName2)
      .on('end', async () => {
        // after the file is saved we read it
        const savedFile = await fs.readFileSync(fileName2)
        if (!savedFile) {
          reject('file can not be read')
        }
        // we have to convert it to base64 in order to send to Google
        const audioBytes = savedFile.toString('base64');
        // this is also a requirement from google
        const audio = {
          content: audioBytes,
        }
        const sttConfig = {
          // if you need punctuation set to true
          enableAutomaticPunctuation: false,
          encoding: "LINEAR16",
          // same rate as we use in our ffmpeg options
          sampleRateHertz: 41000,
          languageCode: "fr-FR",
          model: "default",
          alternativeLanguageCodes: ['en-US']
        }
        // building up the request object
        const request = {
          audio: audio,
          config: sttConfig,
        }

        // now we finally pass it to the Google API and wait for the response
        const [responses] = await client.recognize(request);
        if (!responses) {
          reject('no response')
        }

        console.log(responses.results)
        // iterate through the words and join them to get a string
        const transcript = responses.results
          .map(result => result.alternatives[0].transcript)
          .join('\\n');
        // removing audio files as we don't need them any more
        fs.unlinkSync(fileName)
        fs.unlinkSync(fileName2)
        fs.unlinkSync(tmp_path)

        console.log(`Transcription: ${transcript}`);
        utiles.autoDetectLang(transcript).then(data => {
          response.status(200).json({ "text": transcript, "lang": data})
        })
        

        
        
      })
  
  })
    
    }


const botResponse = async (request, response) => {

  const body = request.body;
  console.log(body)
  let userRequest = body.text

  if (body.lang == lang.EN) {
    userRequest = await utiles.translateText(body.text, lang.FR)
  }

  clientWit.message(userRequest, {}).then( async (data) => {
    

    if (data.intents[0].name == intents.AGE_SEXUALITE_INTENT) {
      //const bot = await utiles.checkResponseLang(returnResponse.AGE_SEXUALITE_RESPONSE, body.lang)
      let [translations] = await translate.translate(returnResponse.AGE_SEXUALITE_RESPONSE, body.lang);
      translations = Array.isArray(translations) ? translations : [translations];
      console.log(translations)
      response.status(200).json({ "text": translations[0], "lang": body.lang })
      
    } else if (data.intents[0].name == intents.BONJOUR_INTENT) {
       const choice = returnResponse.BONJOUR_RESPONSE[utiles.range(0, returnResponse.BONJOUR_RESPONSE.length)]
      // console.log(choice)
      // const bot = await utiles.checkResponseLang(choice, body.lang)
      let [translations] = await translate.translate(choice, body.lang);
      translations = Array.isArray(translations) ? translations : [translations];
      console.log(translations)
      response.status(200).json({ "text": translations[0], "lang": body.lang })
      
    } else if (data.intents[0].name == intents.BONSOIR_INTENT) {
       const choice = returnResponse.BONSOIR_RESPONSE[utiles.range(0, returnResponse.BONSOIR_RESPONSE.length)]
      // const bot = await utiles.checkResponseLang(choice, body.lang)
      let [translations] = await translate.translate(choice, body.lang);
      translations = Array.isArray(translations) ? translations : [translations];
      console.log(translations)
      response.status(200).json({ "text": translations[0], "lang": body.lang })
      
    } else if (data.intents[0].name == intents.CAUSE_DES_PERTE_BLANCHE_INTENT) {
      const choice = returnResponse.CAUSE_DES_PERTE_BLANCHE_RESPONSE
      //const bot = await utiles.checkResponseLang(choice, body.lang)
      let [translations] = await translate.translate(choice, body.lang);
      translations = Array.isArray(translations) ? translations : [translations];
      console.log(translations)
      response.status(200).json({ "text": translations[0], "lang": body.lang })
      
    } else if (data.intents[0].name == intents.CAS_DES_REGLE_DOULOUREUSE_INTENT) {
      const choice = returnResponse.CAS_DES_REGLE_DOULOUREUSE_RESPONSE
     // const bot = await utiles.checkResponseLang(choice, body.lang)
      let [translations] = await translate.translate(choice, body.lang);
      translations = Array.isArray(translations) ? translations : [translations];
      console.log(translations)
      response.status(200).json({ "text": translations[0], "lang": body.lang })
      
    } else if (data.intents[0].name == intents.CAUSE_INFECTION_VAGINALE_INTENT) {
      const choice = returnResponse.CAUSE_INFECTION_VAGINALE_RESPONSE
      let [translations] = await translate.translate(choice, body.lang);
      translations = Array.isArray(translations) ? translations : [translations];
      console.log(translations)
      //const bot = await utiles.checkResponseLang(choice, body.lang)
      response.status(200).json({ "text": translations[0], "lang": body.lang })
      
    } else if (data.intents[0].name == intents.COMMENT_GUERIR_UNE_INFECTION_INTENT) {
      const choice = returnResponse.COMMENT_GUERIR_UNE_INFECTION_RESPONSE
      let [translations] = await translate.translate(choice, body.lang);
      translations = Array.isArray(translations) ? translations : [translations];
      console.log(translations)
      //const bot = await utiles.checkResponseLang(choice, body.lang)
      response.status(200).json({ "text": translations[0], "lang": body.lang })
      
    } else if (data.intents[0].name == intents.CONSEIL_MEDICAL_REGLE_DOULOUREUSE_INTENT) {
      const choice = returnResponse.CONSEIL_MEDICAL_REGLE_DOULOUREUSE_RESPONSE
      let [translations] = await translate.translate(choice, body.lang);
      translations = Array.isArray(translations) ? translations : [translations];
      console.log(translations)
      //const bot = await utiles.checkResponseLang(choice, body.lang)
      response.status(200).json({ "text": translations[0], "lang": body.lang })
      
    } else if (data.intents[0].name == intents.CONSEQUENCE_DE_LA_SEXUALITE_INTENT) {
      const choice = returnResponse.CONSEQUENCE_DE_LA_SEXUALITE_RESPONSE
      let [translations] = await translate.translate(choice, body.lang);
      translations = Array.isArray(translations) ? translations : [translations];
      console.log(translations)
      //const bot = await utiles.checkResponseLang(choice, body.lang)
      response.status(200).json({ "text": translations[0], "lang": body.lang })
      
    } else if (data.intents[0].name == intents.CONSEQUENCE_DES_REGLE_DOULOUREUSE_INTENT) {
      const choice = returnResponse.CONSEQUENCE_DES_REGLE_DOULOUREUSE_RESPONSE
      let [translations] = await translate.translate(choice, body.lang);
      translations = Array.isArray(translations) ? translations : [translations];
      console.log(translations)
      //const bot = await utiles.checkResponseLang(choice, body.lang)
      response.status(200).json({ "text": translations[0], "lang": body.lang })
      
    } else if (data.intents[0].name == intents.DEFINITION_REGLE_DOULOUREUSE_INTENT) {
      const choice = returnResponse.DEFINITION_REGLE_DOULOUREUSE_RESPONSE
      let [translations] = await translate.translate(choice, body.lang);
      translations = Array.isArray(translations) ? translations : [translations];
      console.log(translations)
      //const bot = await utiles.checkResponseLang(choice, body.lang)
      response.status(200).json({ "text": translations[0], "lang": body.lang })
      
    } else if (data.intents[0].name == intents.DIFFERENCE_MST_ET_IST_INTENT) {
      const choice = returnResponse.DIFFERENCE_MST_ET_IST_RESPONSE
      let [translations] = await translate.translate(choice, body.lang);
      translations = Array.isArray(translations) ? translations : [translations];
      console.log(translations)
      //const bot = await utiles.checkResponseLang(choice, body.lang)
      response.status(200).json({ "text": translations[0], "lang": body.lang })
      
    } else if (data.intents[0].name == intents.DEPISTAGE_MST_OU_IST_INTENT) {
      const choice = returnResponse.DEPISTAGE_MST_OU_IST_RESPONSE
      let [translations] = await translate.translate(choice, body.lang);
      translations = Array.isArray(translations) ? translations : [translations];
      console.log(translations)
      //const bot = await utiles.checkResponseLang(choice, body.lang)
      response.status(200).json({ "text": translations[0], "lang": body.lang })
      
    } else if (data.intents[0].name == intents.EVITER_LES_PERTE_BLANCHE_INTENT) {
      const choice = returnResponse.EVITER_LES_PERTE_BLANCHE_RESPONSE
      let [translations] = await translate.translate(choice, body.lang);
      translations = Array.isArray(translations) ? translations : [translations];
      console.log(translations)
      //const bot = await utiles.checkResponseLang(choice, body.lang)
      response.status(200).json({ "text": translations[0], "lang": body.lang })
      
    } else if (data.intents[0].name == intents.FAIRE_SA_TOILETTE_INTENT) {
      const choice = returnResponse.FAIRE_SA_TOILETTE_RESPONSE
      let [translations] = await translate.translate(choice, body.lang);
      translations = Array.isArray(translations) ? translations : [translations];
      console.log(translations)
      //const bot = await utiles.checkResponseLang(choice, body.lang)
      response.status(200).json({ "text": translations[0], "lang": body.lang })
      
    } else if (data.intents[0].name == intents.GROSSESSESSE_PRECOCE_INTENT) {
      const choice = returnResponse.GROSSESSESSE_PRECOCE_RESPONSE
      let [translations] = await translate.translate(choice, body.lang);
      translations = Array.isArray(translations) ? translations : [translations];
      console.log(translations)
      //const bot = await utiles.checkResponseLang(choice, body.lang)
      response.status(200).json({ "text": translations[0], "lang": body.lang })
      
    } else if (data.intents[0].name == intents.INFECTION_VAGINALE_INTENT) {
      const choice = returnResponse.INFECTION_VAGINALE_RESPONSE
      let [translations] = await translate.translate(choice, body.lang);
      translations = Array.isArray(translations) ? translations : [translations];
      console.log(translations)
      //const bot = await utiles.checkResponseLang(choice, body.lang)
      response.status(200).json({ "text": translations[0], "lang": body.lang })
      
    } else if (data.intents[0].name == intents.LA_CONTRACTION_MST_INTENT) {
      const choice = returnResponse.LA_CONTRACTION_MST_RESPONSE
      let [translations] = await translate.translate(choice, body.lang);
      translations = Array.isArray(translations) ? translations : [translations];
      console.log(translations)
      //const bot = await utiles.checkResponseLang(choice, body.lang)
      response.status(200).json({ "text": translations[0], "lang": body.lang })
      
    } else {
      let [translations] = await translate.translate("As tu d'autre question ?", body.lang);
      translations = Array.isArray(translations) ? translations : [translations];
      console.log(translations)
      //const bot = await utiles.checkResponseLang(choice, body.lang)
      response.status(200).json({ "text": translations[0], "lang": body.lang })
    }
  })
  .catch(console.error);



}
      


module.exports = {
  speechToText,
  botResponse
}
